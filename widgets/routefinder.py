from PySide6.QtWidgets import QGroupBox, QLabel, QHBoxLayout, QGridLayout


class RouteFinder:
    def __init__(self, selected_callback, find_route_callback, get_name_callback):
        self.selected_callback = selected_callback
        self.find_route_callback = find_route_callback
        self.get_name_callback = get_name_callback

        self._start = None
        self.widget = RouteFinderWidget(self.get_name_callback)

    def select_route(self, location):
        if self.start is None:
            self.start = location
        elif self.start == location:
            return
        else:
            old = self.start
            self.start = None
            self.find_route_callback(old, location)

    def show_routes(self, routes):
        self.widget.show_routes(routes)

    def cancel(self):
        self.start = None

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        self._start = value
        self.widget.change_text(value)
        self.selected_callback(value, selected=value is not None)


class RouteFinderWidget(QLabel):
    def __init__(self, get_name_callback):
        super().__init__("")
        self.get_name_callback = get_name_callback
        self.setWordWrap(True)

    def show_routes(self, routes):
        t = "\n".join(
            " > ".join(map(self.get_name_callback, route)) for route in routes
        )
        self.setText(t)

    def change_text(self, value=None):
        if value is None:
            self.setText("Route Finder. Alt-Click to find routes.")
        else:
            self.setText(
                f"Finding route from {value.name} to next. Alt-Click on the next"
            )
