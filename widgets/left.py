from PySide6.QtCore import QSize
from PySide6.QtWidgets import QVBoxLayout


class Left(QVBoxLayout):
    def __init__(self):
        super().__init__()

    def minimumSize(self) -> QSize:
        return QSize(900, 100)
