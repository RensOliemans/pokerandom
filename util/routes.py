from itertools import combinations

import networkx as nx

from util.locations import flatten_dict


def create_graph(categories, links):
    G = nx.Graph()
    vertices = {entrance.key for entrance in flatten_dict(categories)}
    G.add_nodes_from(vertices)

    for location in [loc for c in categories for loc in c.locations]:
        G.add_edges_from(combinations([e.key for e in location.entrances], r=2))
    G.add_edges_from(edges_of_links(links))

    return G


def edges_of_links(links):
    for link in links:
        if link.destination is None:
            continue
        yield link.entrance, link.destination


def find_paths(initial, goal, categories, links, n=1):
    g = create_graph(categories, links)
    paths = nx.all_shortest_paths(g, source=initial.key, target=goal.key)
    for x in range(n):
        try:
            yield next(paths)
        except StopIteration:
            return
